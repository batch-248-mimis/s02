CLI - Command Line Interface Commands

GUI is a graphical representation in which the users can interact with software or devices through clickable icons

CLI is a console or text-based representation which a user types commands in a terminal

pwd - present working directory - shows the current folder we are working on

ls - list the files and folders contained by the current directory

mkdir <folderName> - creates a new directory

cd <folderName> - change  directories

touch <fileName> - used to create files

cd - (go back one directory)

cd .. (one folder up)